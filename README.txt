Prudential Control-M_V9

Ansible playbook to download the binaries and XML file of Control-M, untar and to install it. 
Instructions to user:
1. Update the node IP in the inventory file
2. Update the NFS server IP and share path in controlm_config.yml
3. Upload the binaries and xml files of Control-M server, EM, BIM, FORECAST and SELFSERVICE in artifactory
4. Update the artifactory URL and credentials in controlm_config.yml
5. Update the checksum values in controlm_config.yml

Running Playbook:
# ansible-playbook -i inventory controlm_main.yml 

Roles:
Below are the roles used in this playbook

controlm_download_install-- Downloads the binaries and XML files of Control-M server, EM, BIM, FORECAST and SELFSERVICE. Untars the downloaded binaries and install them.

Variables are defined in "controlm_config.yml" ( Artifactory URLs,credentials, app user details, NFS IP and share path details)

Playbook Execution outline:
1. Kills the controlm processes
2. Unmount local disk, install NFS and mounts the remote device
3. Creates a directory to download the binaries and XML files of Control-M server, EM, BIM, FORECAST and SELFSERVICE. Creates respective directories for each binary to get untared. 
4. Installs Control-M server, EM, BIM, FORECAST and SELFSERVICE using the untared binaries and verifies the processes
5. Removes the binaries 


